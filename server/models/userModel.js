const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    players: [
        {
            name: {
                type: String,
            },
            score: {
                type: Number,
            }
        },
        {
            name: {
                type: String,
            },
            score: {
                type: Number,
            }
        }
    ]
}, {
    timestamps: true
});


const User = mongoose.model('user', UserSchema);

module.exports = User;
const User = require('../models/userModel');

const STATUS_SUCCESS = 200;
const STATUS_ERROR = 400;

module.exports.all = (req, res) => {
    User.find(req.body).then((users) => {
        res.status(STATUS_SUCCESS).json(users)
    }).catch((err) => {
        res.status(STATUS_ERROR).json({ error: err.message })
    });

}

module.exports.create = (req, res) => {
    User.create(req.body).then((user) => {
        res.status(STATUS_SUCCESS).json({ result: 'Result Created in Database' })
    }).catch((err) => {
        res.status(STATUS_ERROR).json({ error: err.message })
    });
}

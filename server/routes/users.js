const express = require('express');
const router = express.Router();

const UserController = require('../controllers/userController');

router.get('/scoreboard', UserController.all);
router.post('/result', UserController.create);

module.exports = router;
